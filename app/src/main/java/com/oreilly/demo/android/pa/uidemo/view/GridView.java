package com.oreilly.demo.android.pa.uidemo.view;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;


/**
 * Created by Joseph on 5/1/2017.
 */
/*
public class GridView extends DotView{
   private int cellHeight;
    private int cellWidth;
    private int cellRows = 16;
    private int cellColumns = 16;
    private Paint lines = new Paint();

    public GridView(Context context) {
        super(context);
        init();
    }

    public GridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        lines.setStyle(Paint.Style.FILL_AND_STROKE);
        lines.setColor(Color.BLACK);
        cellWidth = getWidth() / cellColumns;
        cellHeight = getHeight() / cellRows;

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);

        cellWidth = getWidth() / cellColumns;
        cellHeight = getHeight() / cellRows;

        invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(Color.WHITE);

        for (int i = 0; i < cellRows; i++)
        {
            canvas.drawLine(0, i * cellHeight, getWidth(), i * cellHeight,
                    lines);
        }

        for (int i = 0; i < cellColumns; i++)
        {
            canvas.drawLine(i * cellWidth, 0, i * cellWidth, getHeight(),
                    lines);
        }
    }
}*/
