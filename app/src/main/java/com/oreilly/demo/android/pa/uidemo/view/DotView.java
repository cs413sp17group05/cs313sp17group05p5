package com.oreilly.demo.android.pa.uidemo.view;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

import com.oreilly.demo.android.pa.uidemo.model.Dot;
import com.oreilly.demo.android.pa.uidemo.model.Dots;
import static com.oreilly.demo.android.pa.uidemo.constants.Constants.grid_sz;


public class DotView extends View {

    private volatile Dots dots;
    private int g = 10;

    public DotView(Context context) {
        super(context);
        setFocusableInTouchMode(true);
    }

    public DotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusableInTouchMode(true);
    }

    public DotView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFocusableInTouchMode(true);
    }

    public void setDots(Dots dots) { this.dots = dots; }

    public int getIndexX(float x)
    {
        int monsterIndexX;
        float xCalc = x/getWidth()*g;
        monsterIndexX = (int)  xCalc;
        return monsterIndexX;
    }

    public int getIndexY(float y)
    {
        int monsterIndexY;
        float yCalc = y/getHeight()*g;
        monsterIndexY = (int) yCalc;
        return monsterIndexY;
    }


    @Override protected void onDraw(final Canvas canvas) {
        Paint paint = new Paint();
        paint.setStyle(Style.STROKE);
        paint.setColor(Color.BLACK);
        canvas.drawRect(0, 0, getWidth() - 1, getHeight() -1, paint);

        if (null == dots) { return; }

        for(int i =1; i <= g-1; i++){
            canvas.drawLine(0, getHeight()/g*i, getWidth(), getHeight()/g*i, paint);
        }
        for(int i =1; i <= g-1; i++){
            canvas.drawLine(getWidth()/g*i, 0, getWidth()/g*i, getHeight(),paint);
        }

        int halfwidth = getWidth()/g/2;
        int halfheight = getHeight()/g/2;

        paint.setStyle(Style.FILL);
        for (final Dot dot : dots.getDots()) {
            if (dot.getColor() == Color.RED){
                paint.setColor(Color.RED);
            }
            else{
                paint.setColor(Color.GREEN);
            }

            canvas.drawCircle((getWidth()/g)*(dot.getX())+halfwidth,getHeight()/g*dot.getY()+halfheight,dot.getDiameter(),paint);
        }
    }
}
