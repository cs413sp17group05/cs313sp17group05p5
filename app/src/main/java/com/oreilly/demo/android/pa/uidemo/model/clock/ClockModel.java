package com.oreilly.demo.android.pa.uidemo.model.clock;


public interface ClockModel extends OnTickSource {
    void start();
    void stop();
}