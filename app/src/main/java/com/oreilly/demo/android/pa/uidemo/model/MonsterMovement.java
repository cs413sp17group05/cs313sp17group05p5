package com.oreilly.demo.android.pa.uidemo.model;

import java.util.Random;

/**
 * Created by Brendan on 5/3/17.
 */

public class MonsterMovement {
    //grid size
    public int g = 10;

    //matrix of monsters
    private int [][] NewMatrix = new int[g][g];
    //number of monsters
    int numMonsters = 0;

    public int getNumMonsters(){
        return numMonsters;
    }

    public MonsterMovement(){
        Random r = new Random();
        for(int i =0; i<g; i++) {
            for (int j = 0; j < g; j++) {
                int x = r.nextInt(4);
                //to fill about 1/3 of board
               /* if (x == 2) {
                    x = 0;
                }*/
                if (x == 1) {
                    numMonsters++;
                }
                NewMatrix[i][j] = x;
            }
        }
    }

    public int[][] getNewMatrix(){
        return NewMatrix;
    }

    public void setNewMatrix(int[][] m){
        this.NewMatrix = m;
    }

    public int[][] moveMonster() {
        int newX, newY, x, y, z;
        Random r = new Random();
        int newMonsters = 0;
        int[][] skipList = new int[g][g];
        for (int i = 0; i < g; i++) {
            for (int j = 0; j < g; j++) {
                z = 0;
                //checking if there's a monster
                if ((NewMatrix[i][j] == 1 || NewMatrix[i][j] == 2) && skipList[i][j] != 1) {
                    //checking if there's a monster
                    if (NewMatrix[i][j] == 1 || NewMatrix[i][j] == 2) {
                        while (z != 3) {
                            //randomly pick 2 numbers
                            //send monster to i+x, i+y
                            //check if that spot is 1 or 2 already and randomly select again if it is

                            // pick a number, 0-4
                            // if it is 0 or 1, monster is not vulnerable.
                            // if it is 2, monster is vulnerable
                            newMonsters = r.nextInt(4);
                            //if number picked is 0, set it 1
                            if (newMonsters == 0 || newMonsters > 2) {
                                newMonsters = 1;
                            }
                            x = r.nextInt(3);
                            if (x == 2) {
                                x = -1;
                            }
                            y = r.nextInt(3);
                            if (y == 2) {
                                y = -1;
                            }
                            newX = i + x;
                            newY = j + y;
                            //if x or y are zero, and i or j are zero
                            //Make sure not to make it a negative index!
                            if (newX == -1) {
                                newX = g - 1;
                            }
                            //if x or y are zero, and i or j are zero
                            // Make sure to not make it a negative index!
                            if (newY == -1) {
                                newY = g - 1;
                            }
                            //only tries 3 times, in case all neighbors are full.
                            if ((NewMatrix[(newX) % g][(newY) % g] != 1) && (NewMatrix[(newX) % g][(newY) % g] != 2)) {
                                //space is open so monster moves to it
                                NewMatrix[(newX) % g][(newY) % g] = newMonsters;
                                //remove monster
                                NewMatrix[i][j] = 0;
                                //putting a marker so if its moved, doesn't get moved again
                                skipList[(newX) % g][(newY) % g] = 1;
                                z = 3;
                            } else {
                                z++;
                            }
                        }
                    }
                }
            }
        }
        return NewMatrix;
    }

    public void removeMonster(int x, int y){
        NewMatrix[x][y] = 0;
    }

}



